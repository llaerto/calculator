package com.llaerto.calculator;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements MainButtonsFragment.CallBackListener {
    EditText editText;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = new MainButtonsFragment();
            fragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
        editText = (EditText) findViewById(R.id.enter_expression);
        textView = (TextView) findViewById(R.id.answer);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    textView.setText(mathExpression(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public String mathExpression(String expr) {
        String result = textView.getText().toString();
        expr = expr.replaceAll("×", "*");
        Log.d("TAG", expr + " after x*");
        expr = replacePercent(expr);
        Log.d("TAG", expr + " after %");
        expr = replaceSquare(expr);
        Log.d("TAG", expr + " after sq");

        try {
            Expression expression = new ExpressionBuilder(expr).build();
            result = "" + expression.evaluate();
            if (result.substring(result.length() - 2, result.length()).equals(".0"))
                result = result.substring(0, result.length() - 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    String replaceSquare(String expr) {
        String replacement = "";
        for (int symbol = 0; symbol < expr.length(); symbol++) {
            int leftBracket = 0;
            int rightBracket = 0;
            if (expr.charAt(symbol) == '√') {
                replacement = replacement + expr.charAt(symbol);
                expr = expr.substring(0, symbol) + "sqrt(" + expr.substring(symbol + 1, expr.length());
                symbol = symbol + 5;
                for (int i = symbol; i < expr.length(); i++) {
                    if (expr.charAt(i) == '(') {
                        if (leftBracket == 0) {
                            if (i != expr.length() - 1) {
                                expr = expr.substring(0, i) + expr.substring(i + 1, expr.length());
                            } else {
                                expr = expr.substring(0, i);
                            }
                        }
                        leftBracket++;
                    } else if (expr.charAt(i) == ')') {
                        rightBracket++;
                        if (rightBracket == leftBracket) {
                            expr = expr.substring(0, i) + ")" + expr.substring(i, expr.length());
                            break;
                        }
                    } else if (expr.charAt(i) == '+'
                            | expr.charAt(i) == '-'
                            | expr.charAt(i) == '*'
                            | expr.charAt(i) == '/'
                            | expr.charAt(i) == '^') {
                        if (i == expr.length() - 1) {
                            expr = expr.substring(0, i) + ")";
                            break;
                        }
                        if (leftBracket == rightBracket) {
                            expr = expr.substring(0, i) + ")" + expr.substring(i, expr.length());
                            break;
                        }

                    }
                    if (i == expr.length() - 1) {
                        expr = expr + ")";
                        break;
                    }
                }
            }
        }
        return expr;
    }

    String replacePercent(String expr) {
        Character[] chars = {'.', '^', '√', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '(', '%'};
        Character[] numerals = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',};
        Character[] notnum = {')', '^', '(', '+', '-', '√', '%'};
        Character[] badForPlus = {'^', '%'};
        for (int symbol = 0; symbol < expr.length(); symbol++) {
            if (expr.charAt(symbol) == '%') {
                if (symbol < expr.length() - 1) {
                    if (Arrays.asList(chars).contains(expr.charAt(symbol + 1))) {
                        return expr;
                    }
                }
                if (symbol == 0) {
                    return expr;
                }
                for (int i = symbol - 1; i >= 0; i--) {
                    boolean isit = false;
                    Log.d("TAG", "i = " + i + " expr = " + expr + " char at = " + expr.charAt(i));
                    if (expr.charAt(i) == '*' | expr.charAt(i) == '/') {
                        expr = expr.substring(0, symbol) + expr.charAt(i) + "0.01" + expr.substring(symbol + 1, expr.length());
                        symbol = symbol + 4;
                        break;
                    }
                    if (expr.charAt(i) == '+' | expr.charAt(i) == '-') {
                        int leftBracket = 0;
                        int rightBracket = 0;
                        int numcount = 0;
                        String replace = "";
                        for (int j = i - 1; j >= 0; j--) {
                            Log.d("TAG", "j = " + j + " replace = " + replace + " expr = " + expr + " char at = " + expr.charAt(j));
                            if (Arrays.asList(badForPlus).contains(expr.charAt(j)) && numcount == 0) {
                                return expr;
                            }
                            if (expr.charAt(j) == ')') {
                                replace = expr.charAt(j) + replace;
                                rightBracket++;

                            }
                            if (Arrays.asList(numerals).contains(expr.charAt(j))) {
                                replace = expr.charAt(j) + replace;
                                Log.d("TAG", "in numerals j = " + j + " replace = " + replace + " expr = " + expr + " char at = " + expr.charAt(j));
                                numcount++;
                                if (j == 0) {
                                    expr = expr.substring(0, symbol) + "*0.01*" + replace + expr.substring(symbol + 1, expr.length());
                                    symbol = symbol + 4 + replace.length();
                                    isit = true;
                                    Log.d("TAG", "i = " + i + " j = " + j + " expr = " + expr);
                                    break;
                                }
                            }
                            if (expr.charAt(j) == '.' && numcount > 0) {
                                replace = expr.charAt(j) + replace;
                            }

                            if (expr.charAt(j) == '(' && numcount > 0) {
                                Log.d("TAG", "tyt");
                                replace = expr.charAt(j) + replace;
                                leftBracket++;
                                if (rightBracket == leftBracket) {
                                    expr = expr.substring(0, symbol) + "*0.01*" + replace + expr.substring(symbol + 1, expr.length());
                                    symbol = symbol + 4 + replace.length();
                                    isit = true;
                                    break;

                                }
                            }
                            if ((expr.charAt(j) == '*' | expr.charAt(j) == '/' | expr.charAt(j) == '√') && numcount > 0) {
                                replace = expr.charAt(j) + replace;
                            }
                            if (expr.charAt(j) == '+' | expr.charAt(i) == '-') {
                                if (leftBracket == rightBracket) {
                                    expr = expr.substring(0, symbol) + "*0.01*" + replace + expr.substring(symbol + 1, expr.length());
                                    symbol = symbol + 4 + replace.length();
                                    isit = true;
                                    break;
                                } else {
                                    replace = expr.charAt(j) + replace;
                                }
                            }

                        }
                        if (isit)
                            break;
                    }
                    Log.d("TAG", "i = " + i + " expr = " + expr + " char at = " + expr.charAt(i));
                    if (Arrays.asList(notnum).contains(expr.charAt(i))) {
                        return expr;
                    }
                }
            }

        }
        Log.d("TAG", expr);
        return expr;
    }


    @Override
    public void clear() {
        editText.setText("");
        textView.setText("");
    }

    @Override
    public void clearOne() {
        if (editText.length() > 0) {
            editText.setText(editText.getText().toString().substring(0, editText.length() - 1));
            if (editText.length() == 0) {
                textView.setText("");
            }
        }
    }

    @Override
    public void equal() {
        String x = mathExpression(editText.getText().toString());
        editText.setText(x);
        textView.setText(x);
    }


    @Override
    public void setText(String setIt) {
        String fulltext = editText.getText().toString() + setIt;
        editText.setText(fulltext);
    }
}
