package com.llaerto.calculator;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


public class MainButtonsFragment extends Fragment implements View.OnClickListener {
    CallBackListener callBackListener;
    Button button0, button1, button2, button3, button4, button5, button6, button7, button8, button9,
            buttonDot, buttonPercent, buttonClear, buttonBackspace, buttonLBracket, buttonRBracket, buttonDivide,
            buttonMultiple, buttonPlus, buttonMinus, buttonEqual, buttonSquare, buttonDegree, buttonMore, buttonQuest;
    Button[] buttons = {button0, button1, button2, button3, button4, button5, button6, button7, button8, button9,
            buttonDot, buttonPercent, buttonClear, buttonBackspace, buttonLBracket, buttonRBracket, buttonDivide,
            buttonMultiple, buttonPlus, buttonMinus, buttonEqual, buttonSquare, buttonDegree, buttonMore, buttonQuest};
    public interface CallBackListener {
        void clear();

        void clearOne();

        void equal();

//        void other();

        void setText(String setIt);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callBackListener = (CallBackListener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initializeButtons(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(10);
        switch (v.getId()) {
            case R.id.Clear:
                callBackListener.clear();
                break;
            case R.id.Clear_one:
                callBackListener.clearOne();
                break;
            case R.id.equal:
                callBackListener.equal();
                break;
            case R.id.history:
                Toast.makeText(getActivity(), R.string.in_maintenance, Toast.LENGTH_SHORT).show();
                break;
            case R.id.other:
                Toast.makeText(getActivity(), R.string.in_maintenance, Toast.LENGTH_SHORT).show();
                break;
            case R.id.left_bracket:
                callBackListener.setText("(");
                break;
            case R.id.right_bracket:
                callBackListener.setText(")");
                break;
            case R.id.seven:
                callBackListener.setText("7");
                break;
            case R.id.eight:
                callBackListener.setText("8");
                break;
            case R.id.nine:
                callBackListener.setText("9");
                break;
            case R.id.division:
                callBackListener.setText("/");
                break;
            case R.id.square:
                callBackListener.setText("√");
                break;
            case R.id.four:
                callBackListener.setText("4");
                break;
            case R.id.five:
                callBackListener.setText("5");
                break;
            case R.id.six:
                callBackListener.setText("6");
                break;
            case R.id.multiplicate:
                callBackListener.setText("×");
                break;
            case R.id.degree:
                callBackListener.setText("^");
                break;
            case R.id.one:
                callBackListener.setText("1");
                break;
            case R.id.two:
                callBackListener.setText("2");
                break;
            case R.id.three:
                callBackListener.setText("3");
                break;
            case R.id.minus:
                callBackListener.setText("-");
                break;
            case R.id.zero:
                callBackListener.setText("0");
                break;
            case R.id.dot:
                callBackListener.setText(".");
                break;
            case R.id.percent:
                callBackListener.setText("%");
                break;
            case R.id.plus:
                callBackListener.setText("+");
                break;


        }
    }

    void initializeButtons (View view) {
        button0 = (Button) view.findViewById(R.id.zero);
        button0.setOnClickListener(this);
        button1 = (Button) view.findViewById(R.id.one);
        button1.setOnClickListener(this);
        button2 = (Button) view.findViewById(R.id.two);
        button2.setOnClickListener(this);
        button3 = (Button) view.findViewById(R.id.three);
        button3.setOnClickListener(this);
        button4 = (Button) view.findViewById(R.id.four);
        button4.setOnClickListener(this);
        button5 = (Button) view.findViewById(R.id.five);
        button5.setOnClickListener(this);
        button6 = (Button) view.findViewById(R.id.six);
        button6.setOnClickListener(this);
        button7 = (Button) view.findViewById(R.id.seven);
        button7.setOnClickListener(this);
        button8 = (Button) view.findViewById(R.id.eight);
        button8.setOnClickListener(this);
        button9 = (Button) view.findViewById(R.id.nine);
        button9.setOnClickListener(this);
        buttonBackspace = (Button) view.findViewById(R.id.Clear_one);
        buttonBackspace.setOnClickListener(this);
        buttonClear = (Button) view.findViewById(R.id.Clear);
        buttonClear.setOnClickListener(this);
        buttonDegree = (Button) view.findViewById(R.id.degree);
        buttonDegree.setOnClickListener(this);
        buttonDivide = (Button) view.findViewById(R.id.division);
        buttonDivide.setOnClickListener(this);
        buttonDot = (Button) view.findViewById(R.id.dot);
        buttonDot.setOnClickListener(this);
        buttonEqual = (Button) view.findViewById(R.id.equal);
        buttonEqual.setOnClickListener(this);
        buttonLBracket = (Button) view.findViewById(R.id.left_bracket);
        buttonLBracket.setOnClickListener(this);
        buttonRBracket = (Button) view.findViewById(R.id.right_bracket);
        buttonRBracket.setOnClickListener(this);
        buttonMinus = (Button) view.findViewById(R.id.minus);
        buttonMinus.setOnClickListener(this);
        buttonMore = (Button) view.findViewById(R.id.other);
        buttonMore.setOnClickListener(this);
        buttonMultiple = (Button) view.findViewById(R.id.multiplicate);
        buttonMultiple.setOnClickListener(this);
        buttonPercent = (Button) view.findViewById(R.id.percent);
        buttonPercent.setOnClickListener(this);
        buttonPlus = (Button) view.findViewById(R.id.plus);
        buttonPlus.setOnClickListener(this);
        buttonQuest = (Button) view.findViewById(R.id.history);
        buttonQuest.setOnClickListener(this);
        buttonSquare = (Button) view.findViewById(R.id.square);
        buttonSquare.setOnClickListener(this);

    }
}
